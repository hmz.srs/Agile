import {CuisineComponent} from "./cuisine/cuisine.component";
import {Routes} from "@angular/router";

export const ROUTES: Routes = [
  { path: '', component: CuisineComponent }
];
