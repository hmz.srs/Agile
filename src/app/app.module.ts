import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {CuisineComponent} from './cuisine/cuisine.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {RouterModule} from "@angular/router";
import {ROUTES} from "./app.routes";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { FrigoComponent } from './cuisine/frigo/frigo.component';
import { LaveVaiselleComponent } from './cuisine/lave-vaiselle/lave-vaiselle.component';
import { MicroOndeComponent } from './cuisine/micro-onde/micro-onde.component';
import { HotteComponent } from './cuisine/hotte/hotte.component';
import { ModalComponent } from './cuisine/modal/modal.component';


@NgModule({
  declarations: [
    AppComponent,
    CuisineComponent,
    SidebarComponent,
    FrigoComponent,
    LaveVaiselleComponent,
    MicroOndeComponent,
    HotteComponent,
    ModalComponent
  ],
  imports: [ BrowserModule, RouterModule.forRoot(ROUTES),BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
