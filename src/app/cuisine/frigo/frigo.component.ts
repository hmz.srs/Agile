import { Component, OnInit } from '@angular/core';
import {style, animate, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-frigo',
  templateUrl: './frigo.component.html',
  styleUrls: ['./frigo.component.css'],
  animations: [
    trigger('testAnimation', [
      transition('* => fadeIn', [
        style({opacity: 0}),

        animate(1000, style({opacity: 1}))
      ]),
      transition('* => fadeOut', [
        animate(1000, style({opacity: 0}))
      ])
      ])
  ]
})
export class FrigoComponent implements OnInit {

  display :boolean =false;

  bindingVar = 'fadeIn';

  constructor() { }

  ngOnInit() {
  }

}
