import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotteComponent } from './hotte.component';

describe('HotteComponent', () => {
  let component: HotteComponent;
  let fixture: ComponentFixture<HotteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
