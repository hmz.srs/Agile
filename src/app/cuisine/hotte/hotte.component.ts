import { Component, OnInit } from '@angular/core';
import {style, animate, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-hotte',
  templateUrl: './hotte.component.html',
  styleUrls: ['./hotte.component.css'],
  animations: [
    trigger('testAnimation', [
      transition('* => fadeIn', [
        style({opacity: 0}),
        animate(1000, style({opacity: 1}))
      ]),
      transition('* => fadeOut', [
        animate(1000, style({opacity: 0}))
      ])
    ])
  ]
})
export class HotteComponent implements OnInit {

  display :boolean =false;

  bindingVar = 'fadeIn';


  constructor() { }

  ngOnInit() {
  }

}
