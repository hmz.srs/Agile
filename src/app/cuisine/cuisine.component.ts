import {Component, OnInit, ViewChild} from '@angular/core';
import {transition, trigger, style, animate} from "@angular/animations";
import {ModalComponent} from "./modal/modal.component";

@Component({
  selector: 'app-cuisine',
  templateUrl: './cuisine.component.html',
  styleUrls: ['./cuisine.component.css'],
  animations: [
    trigger('testAnimation', [
      transition('* => fadeIn', [
        style({ opacity: 0 }),
        animate(500)
      ]),
      transition('* => fadeOut', [
        animate(500)
      ])
    ])
  ]
})
export class CuisineComponent implements OnInit {

  constructor() { }

  bindingVar = 'fadeIn';

  @ViewChild('modalFrigo') readonly modalFrigo: ModalComponent;


  test : boolean = false;

  ngOnInit() {
  }

  openModal(){
    console.log("test");
    this.modalFrigo.openModal();
  }

}
