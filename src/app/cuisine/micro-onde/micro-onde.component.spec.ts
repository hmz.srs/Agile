import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroOndeComponent } from './micro-onde.component';

describe('MicroOndeComponent', () => {
  let component: MicroOndeComponent;
  let fixture: ComponentFixture<MicroOndeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicroOndeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroOndeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
