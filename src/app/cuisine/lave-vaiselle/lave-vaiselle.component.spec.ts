import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaveVaiselleComponent } from './lave-vaiselle.component';

describe('LaveVaiselleComponent', () => {
  let component: LaveVaiselleComponent;
  let fixture: ComponentFixture<LaveVaiselleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaveVaiselleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaveVaiselleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
