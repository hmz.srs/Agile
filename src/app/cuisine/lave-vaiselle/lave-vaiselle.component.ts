import { Component, OnInit } from '@angular/core';
import {style, animate, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-lave-vaiselle',
  templateUrl: './lave-vaiselle.component.html',
  styleUrls: ['./lave-vaiselle.component.css'],
  animations: [
    trigger('testAnimation', [
      transition('* => fadeIn', [
        style({ opacity: 0 }),
        animate(500)
      ]),
      transition('* => fadeOut', [
        animate(500)
      ])
    ])
  ]
})
export class LaveVaiselleComponent implements OnInit {
  display :boolean =false;
  bindingVar = 'fadeIn';
  constructor() { }

  ngOnInit() {
  }

}
